﻿using UnityEngine;
using System.Collections;

namespace gc {
    public class TaskTest : MonoBehaviour {

        TaskManager tm = new TaskManager();

        ActionTask sayHello = new ActionTask(() => {print("Hello");});
        ActionTask sayGoodbye = new ActionTask(() => { Debug.Log("Goodbye"); });
        
        // Use this for initialization
        void Start() {

            Debug.Log(sayHello);
            sayHello.Then(sayGoodbye);
            tm.AddTask(sayHello);
        }

        // Update is called once per frame
        void Update() {
            tm.Update();
        }
    }
}
