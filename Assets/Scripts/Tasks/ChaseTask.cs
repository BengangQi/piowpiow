﻿using UnityEngine;
using System;

namespace gc
{
    public class ChaseTask : Task{

        Vector3 minionPos1;
        Vector3 minionPos2;

        protected override void Init()
        {
            Debug.Log("Chase");
            BossEnemy._instance.GetComponent<EnemyMovement>().enabled = true;
        }

        internal override void Update()
        {
            if (SpawnTask.minionNum == 0)
            {
                MinionGen();
            }
        }

        private void MinionGen()
        {
            minionPos1.x = BossEnemy.bossPos.x - 10;
            minionPos1.y = BossEnemy.bossPos.y;
            minionPos1.z = BossEnemy.bossPos.z;

            minionPos2.x = BossEnemy.bossPos.x + 10;
            minionPos2.y = BossEnemy.bossPos.y;
            minionPos2.z = BossEnemy.bossPos.z;
            Spawner.Spawn("Prefabs/minion", minionPos1);
            Spawner.Spawn("Prefabs/minion", minionPos2);
            SpawnTask.minionNum = SpawnTask.minionNum + 2;
        }
    }
}
