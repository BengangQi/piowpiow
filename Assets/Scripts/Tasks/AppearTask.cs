﻿using UnityEngine;
using System;

namespace gc{

    public class AppearTask : Task 
    {

        private Vector3 _position = new Vector3 (60, 45, 0);
        private float _startTime;
        private int testint;
        private float spawnDuration = 2f;
        protected float startScale = 0.01f;
        protected float startAlpha = 0.1f;
        protected GameObject _boss;
        public static Vector3 bossPos;

        protected override void Init(){

            EnemyManager.instance.enemyCount(1);
            EnemyManager.instance.waveCount(1);
            // Get Boss spawn time (for enlarge animation)
            _startTime = Time.time;
            //Boss spawn
            _boss = bossSpawn("Prefabs/boss_n", _position);
            Debug.Log(_boss);
        }

        internal override void Update()
        {
            float elapsedTime = Time.time - _startTime;
            float normalizedTime = elapsedTime / spawnDuration;

            if (normalizedTime > 1)
            {
                FinishAppearing();
            }
            else
            {
                UpdateSpawnAnimation(normalizedTime);
            }
        }
        protected GameObject bossSpawn(string resourceName, Vector3 position)
        {
            UnityEngine.Object resource = ResourceLoader.Instance.GetResource(resourceName);
			GameObject objectToSpawn = GameObject.Instantiate(resource, position, Quaternion.identity) as GameObject;

            // turn off all the scripts on the object besides the renderer
            MonoBehaviour[] scripts = objectToSpawn.GetComponents<MonoBehaviour>();
            foreach (MonoBehaviour script in scripts)
            {
                if (script.GetType() != typeof(SpriteRenderer))
                {
                    script.enabled = false;
                }
            }
            // Return the gameobject that just respawned
            return objectToSpawn;
        }

        private void FinishAppearing()
        {
            // Activate all the scripts that were turned off at the start
            EnableAllScripts();
            // Make sure the scale and alpha are set to their maximum values before finishing
            UpdateSpawnAnimation(1);
            // Finish Task
            SetStatus(TaskStatus.Success);
            // Add next task
            SpawnTask _task = new SpawnTask();
            TaskManager.Instance().AddTask(_task);
        }

        private void EnableAllScripts()
        {
            // turn off all the scripts on the object besides the renderer
            /*
            MonoBehaviour[] scripts = _boss.GetComponents<MonoBehaviour>();
            foreach (MonoBehaviour script in scripts)
            {
                script.enabled = true;
            }
            */
            // Enable Boss enemy script

            _boss.GetComponent<BossEnemy>().enabled = true;
            // Enable collider2D for boss
            _boss.GetComponent<Collider2D>().enabled = true;
        }

        private void UpdateSpawnAnimation(float normalizedTime)
        {
            // NOTE: this assumes that the spawned objects always have white as their sprite color and have a scale of 1
            SpriteRenderer renderer = _boss.GetComponent<SpriteRenderer>();
            float alpha = (1 - startAlpha) * normalizedTime + startAlpha;
            renderer.color = new Color(1, 1, alpha);
            float scale = (2 - startScale) * normalizedTime + startScale;
            _boss.transform.localScale = new Vector3(scale, scale, 1);
        }
    }
}
