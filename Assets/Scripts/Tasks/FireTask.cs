﻿using UnityEngine;
using System.Collections;

namespace gc
{
    public class FireTask : Task
    {
        Vector2 randomPos;
        Vector3 bossPos;

        protected override void Init()
        {
            Debug.Log("Fire");
        }

        internal override void Update()
        {
            randomPos = new Vector2(Random.value, Random.value);
            BossFire();

            if (BossEnemy.bossHP < 350)
            {
                FinishFiring();
            }
        }

        private void FinishFiring()
        {
            // Finish Task
            SetStatus(TaskStatus.Success);
            // Add next task
            ChaseTask _task = new ChaseTask();
            TaskManager.Instance().AddTask(_task);
        }

        void BossFire()
        {
            Debug.Log("BossFire");
            Bullet.Fire(randomPos, BossEnemy.bossPos, 1);
        }
    }
}
