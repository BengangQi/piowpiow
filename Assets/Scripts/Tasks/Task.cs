﻿using UnityEngine;
using System;

namespace gc
{
    public class Task
    {
        public enum TaskStatus : byte
        {
            Detached,
            Pending,
            Working,
            Success,
            Fail,
            Aborted
        }

        public TaskStatus Status { get; private set; }

        public bool IsDetached
        { get { return Status == TaskStatus.Detached; } }
        public bool IsAttached
        { get { return Status != TaskStatus.Detached; } }
        public bool IsPending
        { get { return Status == TaskStatus.Pending; } }
        public bool IsWorking
        { get { return Status == TaskStatus.Working; } }
        public bool IsSuccessful
        { get { return Status == TaskStatus.Success; } }
        public bool IsFailed
        { get { return Status == TaskStatus.Fail; } }
        public bool IsAborted
        { get { return Status == TaskStatus.Aborted; } }

        public bool IsFinished
        { get { return (Status == TaskStatus.Fail || Status == TaskStatus.Success || Status == TaskStatus.Aborted); } }

        public void Abort()
        {
            SetStatus(TaskStatus.Aborted);
        }

        internal void SetStatus(TaskStatus newStatus)
        {
            if (Status == newStatus) return;

            Status = newStatus;

            switch (newStatus)
            {
                case TaskStatus.Working:
                    Init();
                    break;

                case TaskStatus.Success:
                    OnSuccess();
                    CleanUp();
                    break;

                case TaskStatus.Aborted:
                    OnAbort();
                    CleanUp();
                    break;
                case TaskStatus.Detached:
                case TaskStatus.Pending:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(newStatus.ToString(), newStatus, null);
            }
        }

        protected virtual void OnAbort() { }
        protected virtual void OnSuccess() { }
        protected virtual void OnFail() { }

        protected virtual void Init() { }
        internal virtual void Update() { }
        protected virtual void CleanUp() { }

        public Task NextTask { get; private set; }

        public Task Then(Task task)
        {
            Debug.Assert(!task.IsAttached);
            NextTask = task;
            return task;
        }

        public Task Then(Action action)
        {
            ActionTask task = new ActionTask(action);
            return Then(task);
        }
    }

    public class ActionTask : Task
    {
        private readonly Action _action;

        public ActionTask(Action action)
        {
            _action = action;
        }
        protected override void Init()
        {
            SetStatus(TaskStatus.Success);
            _action();
        }
    }
}
