﻿using UnityEngine;
using System;

namespace gc
{  
    public class TestTask : Task {
        private readonly Action _action;

        public TestTask()
        {
        
        }
        
        protected override void Init(){
            Debug.Log("Boss coming");
            SetStatus(TaskStatus.Success);
        }
    }
}
