﻿using UnityEngine;
using System;
using System.Collections;

namespace gc
{
    public class SpawnTask : Task
    {
        Vector3 minionPos1;
        Vector3 minionPos2;

        //number of minions
        public static int minionNum;

        protected override void Init()
        {
            Debug.Log("Spawn");
        }

        internal override void Update()
        {
            if (minionNum == 0)
            {
                MinionGen();
            }

            if (BossEnemy.bossHP < 1500)
            {
                FinishSpawning();
            }
        }

        private void FinishSpawning()
        {
            // Finish Task
            SetStatus(TaskStatus.Success);
            // Add next task
            FireTask _task = new FireTask();
            TaskManager.Instance().AddTask(_task);
        }

        private void MinionGen()
        {
            minionPos1.x = BossEnemy.bossPos.x - 10;
            minionPos1.y = BossEnemy.bossPos.y;
            minionPos1.z = BossEnemy.bossPos.z;

            minionPos2.x = BossEnemy.bossPos.x + 10;
            minionPos2.y = BossEnemy.bossPos.y;
            minionPos2.z = BossEnemy.bossPos.z;
            Spawner.Spawn("Prefabs/minion", minionPos1);
            Spawner.Spawn("Prefabs/minion", minionPos2);
            minionNum = minionNum + 2;
        }
    }
}
