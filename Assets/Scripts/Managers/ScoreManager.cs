﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {

    public static int score = 0;

    // Use this for initialization
    void Start () {
        EventManager.Instance.Register<EnemyDieEvent>(EnemyScoreEvent);
	}

    public void EnemyScoreEvent(Event e)
    {
        EnemyDieEvent enemyscore = (EnemyDieEvent)e;
        score += enemyscore.score;
    }
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(score);
	}

    void OnDestroy()
    {
        EventManager.Instance.Unregister<EnemyDieEvent>(EnemyScoreEvent);
    }
}
