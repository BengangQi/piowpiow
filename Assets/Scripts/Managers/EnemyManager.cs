using UnityEngine;

namespace gc
{
    public class EnemyManager : MonoBehaviour
    {
        private int enemyNum;
        private int EnemySpawn;
        private int MinNumEnemies;

        public static int testnum = 5;

        public static int Wave = 0;

        public static EnemyManager instance;

        void Awake()
        {
            instance = this;
        }

        void Start()
        {
            Debug.Log(enemyNum);
            // Spawn enemies
            groupGen();
        }

        void FixedUpdate()
        {
            if(enemyNum == 0 && (Wave % 3) != 0)
            {
                //groupGen();
                bossGen();
            }else if(enemyNum == 0 && (Wave % 3) == 0)
            {
                bossGen();
            }
        }

        // generate enemies as a group
        void groupGen()
        {
            Wave = Wave + 1;
            MinNumEnemies = Random.Range(3, 6);

            for (int i = 0; i < MinNumEnemies; i++)
            {
                EnemySpawn = Random.Range(0, 3);
                //Debug.Log(EnemySpawn);

                if (EnemySpawn == 1)
                {
                    Spawner.Spawn("Prefabs/Enemyship", Game.RandomWorldPosition(3));
                }
                else if (EnemySpawn == 2)
                {
                    Spawner.Spawn("Prefabs/Enemyship2", Game.RandomWorldPosition(3));
                }
                else {
                    Spawner.Spawn("Prefabs/Enemyship3", Game.RandomWorldPosition(3));
                }
            }
        }

        void bossGen()
        {
            AppearTask _task = new AppearTask();
            TaskManager.Instance().AddTask(_task);

            //Spawner.Spawn("Prefabs/boss_m", Game.RandomWorldPosition(3));
        }

        public void enemyCount(int num)
        {
            enemyNum = enemyNum + num;
        }

        public void waveCount(int num)
        {
            Wave = Wave + num;
        }

        void OnGUI()
        {
            GUI.Label(new Rect(10, 10, 400, 100), "Enemy Number:" + enemyNum);
            GUI.Label(new Rect(10, 30, 400, 100), "Wave:" + Wave);
            GUI.Label(new Rect(10, 50, 400, 100), "Score:" + ScoreManager.score);
        }

    }
}