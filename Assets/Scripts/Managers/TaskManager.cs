﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace gc
{
	public class TaskManager : MonoBehaviour
    {
        // make a singleton first
        private static TaskManager instance;
        public static TaskManager Instance()
        {
            if (instance != null)
            {
                return instance;
            }
            else {
                //If it doesn't exist, make one and return that.
                GameObject TaskManageObject = new GameObject();
                instance = TaskManageObject.AddComponent<TaskManager>();
                return instance;
            }
        }

        private readonly List<Task> _tasks = new List<Task>();

        //add task
        public void AddTask(Task task)
        {
			Debug.Assert(task != null);
            Debug.Assert(!task.IsAttached);
            _tasks.Add(task);
            task.SetStatus(Task.TaskStatus.Pending);
        }

        // update
        // manages all tasks' lifecycle
        public void Update()
        {
            //execute for each task
            for (int i = _tasks.Count - 1; i >= 0; --i)
            {
                Task task = _tasks[i];
                //initialize target
                if (task.IsPending)
                {
                    task.SetStatus(Task.TaskStatus.Working);
                }

                //finish task 
                if (task.IsFinished)
                {
                    HandleCompletion(task, i);
                }
                else
                {
                    // update the task
                    task.Update();
                    if (task.IsFinished)
                    {
                        HandleCompletion(task, i);
                    }
                }
            }
        }

        //Handle Completion
        private void HandleCompletion(Task task, int taskIndex)
        {
            if (task.NextTask != null && task.IsSuccessful)
            {
                AddTask(task.NextTask);
            }
            //remove task from queue
            _tasks.RemoveAt(taskIndex);
            task.SetStatus(Task.TaskStatus.Detached);
        }

        //Abort tasks no matter if there're finished?
        public void AbortAll<T>() where T : Task
        {
            Type type = typeof(T);
            //go backwards throuh the list again
            for (int i = _tasks.Count - 1; i >= 0; --i)
            {
                Task taskToAbort = _tasks[i];
                //Safety check to make sure you're aborting a Task type
                if (taskToAbort.GetType() == type)
                {
                    taskToAbort.Abort();
                }
            }
        }
    }
}