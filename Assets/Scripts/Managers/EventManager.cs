﻿using UnityEngine;
using System;
using System.Collections.Generic;

public abstract class Event
{
    public delegate void Handler(Event e);
}

public class EventManager : MonoBehaviour
{
    // Just some singleton boiler plate
    static private EventManager instance;
    static public EventManager Instance
    {
        get
        {
            return instance;
        }
    }

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    // Here is the core of the system: a dictionary that maps Types (specifically Event types in this case)
    // to Event.Handlers
    private Dictionary<Type, Event.Handler> registeredHandlers = new Dictionary<Type, Event.Handler>();

    public void Register<T>(Event.Handler handler) where T : Event
    {

        Type type = typeof(T);
        if (registeredHandlers.ContainsKey(type))
        {
            registeredHandlers[type] += handler;
        }
        else {
            registeredHandlers[type] = handler;
        }
    }

    public void Unregister<T>(Event.Handler handler) where T : Event
    {
        Type type = typeof(T);
        Event.Handler handlers;
        if (registeredHandlers.TryGetValue(type, out handlers))
        {
            handlers -= handler;
            if (handlers == null)
            {
                registeredHandlers.Remove(type);
            }
            else {
                registeredHandlers[type] = handlers;
            }
        }
    }
    // This is how you "publish" and event. All it entails is looking up
    // the event type and calling the delegate containing all the handlers
    public void Fire(Event e)
    {
        Type type = e.GetType();
        Event.Handler handlers;
        if (registeredHandlers.TryGetValue(type, out handlers))
        {
            handlers(e);
        }
    }
}
