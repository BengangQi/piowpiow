﻿using UnityEngine;

namespace gc {

	public class Game : MonoBehaviour
    {
        public static int score = 0;

		void Update () {
			// Spawn player
			if (Player.Instance == null) {
				Spawner.Spawn("Prefabs/Playership", RandomWorldPosition(3));	
			}
        }			
		    public static Vector3 RandomWorldPosition(int gutter) {
			return new Vector3(Random.Range(gutter, Config.WorldWidth - gutter), Random.Range(gutter, Config.WorldHeight - gutter), 0); 
		}
	}
}

