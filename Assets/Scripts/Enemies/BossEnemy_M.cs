﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;

namespace gc
{
    public class BossEnemy_M : Enemy
    {
        public static BossEnemy_M _instance_m;
        public static int bossmHP;
        private FSM<BossEnemy_M> _states;
        public static Vector2 bossmPos;
        public static bool isHit = false;
        public static bool isPrepFinished = false;


        public BossEnemy_M()
        {
            _states = new FSM<BossEnemy_M>(this);
            _states.TransitionTo<Seeking>();
        }

        private class Seeking : FSM<BossEnemy_M>.State
        {
            public override void Init()
            {
                Debug.Log("Seeking");
            }
            private Vector2 _distance;

            public override void Update()                        
            {
                _distance = bossmPos - Player.playerPos;
                if (_distance.magnitude < 50)
                {
                    TransitionTo<Preparation>();
                }
            }
        }

        private class Preparation : FSM<BossEnemy_M>.State
        {
            int hpMark = bossmHP;

            public override void Init()
            {
                Debug.Log("Preparation");
                hpMark = bossmHP;
            }

            public override void Update()
            {
                for (int i = 0; i < 5; i++)
                {
                    if (bossmHP >= hpMark)
                    {
                        Context.StartCoroutine(Pulse());
                    }
                    else if (bossmHP < hpMark)
                    {
                        TransitionTo<Fleeing>();
                    }
                    else if (bossmHP >= hpMark && i == 5)
                    {
                        TransitionTo<Attack>();
                    }
                }
            }

            IEnumerator Pulse()
            {
                BossEnemy_M._instance_m.GetComponent<EnemyMovement>().enabled = false;
                yield return new WaitForSeconds(2); 
                BossEnemy_M._instance_m.GetComponent<EnemyMovement>().enabled = true;
            }
        }

        private class Attack : FSM<BossEnemy_M>.State
        {
            Vector2 temp_playerPos = Player.playerPos;


            public override void Init()
            {
                Debug.Log("Attack");
                temp_playerPos = Player.playerPos;
                BossEnemy_M._instance_m.GetComponent<EnemyMovement>().MaxSpeed = 120;
            }

            public override void Update()
            {
                Vector2 _OffsetToPlayer = temp_playerPos - bossmPos;
                float _distance = _OffsetToPlayer.magnitude;

                if (_distance < 5)
                {
                    BossEnemy_M._instance_m.GetComponent<EnemyMovement>().MaxSpeed = 60;
                    TransitionTo<Seeking>();
                }
            }

            void OnTriggerEnter2D(Collider2D coll)
            {
                Debug.Log("Hit");
            }

        }

        private class Fleeing : FSM<BossEnemy_M>.State
        {




        }


        // Use this for initialization
        void Start()
        {
            _instance_m = this;
            bossmPos = this.transform.position;
            bossmHP = this.Health;
        }

        // Update is called once per frame
        void Update()
        {
            _states.Update();
            bossmPos = this.transform.position;
            bossmHP = this.Health;
        }

        protected override void genSound()
        {
            AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/boss_appear");
            AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
        }
    }
}
