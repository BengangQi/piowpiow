﻿using UnityEngine;
using UnityEngine.Assertions;

namespace gc {

	public class Enemy : MonoBehaviour {
	
		public static int NumEnemies { get; private set; }

		public int Health;

		public int Damage;

        public int enemyScore = 5;

		void Awake() {
            genSound();
            EnemyManager.instance.enemyCount(1);
		}
        //play sound when enemy generate
        protected virtual void genSound()
        {
			AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/enemy_appear");
			AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);            
        }

		void Update() {
			if (Health <= 0) {
				Destroy(gameObject);
			}
		}

		void OnCollisionEnter2D(Collision2D coll) {
			if (coll.gameObject.CompareTag("PLAYER")) {
				Player player = coll.gameObject.GetComponent<Player>();
				Assert.IsNotNull(player, "Object tagged PLAYER but no Player script attached");
				player.SendMessage("ApplyDamage", Damage); 
			}
		}

		protected virtual void ApplyDamage(int damage) {
			Health -= damage;
			if (Health <= 0) {
				AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/enemy_destroyed_1");
				AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
			}
		}

        protected virtual void OnDestroy() {
            EnemyManager.instance.enemyCount(-1);
            EventManager.Instance.Fire(new EnemyDieEvent(enemyScore));
        }
	}

}

