using UnityEngine;

namespace gc
{
    public class dashEnemy: Enemy{
        protected override void ApplyDamage(int damage) {
			Health -= damage;
			if (Health <= 0) {
				AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/enemy_destroyed_2");
				AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
			}
		}
    }
}