﻿using UnityEngine;
namespace gc
{
    public class BossEnemy : Enemy
    {

        public static BossEnemy _instance;
        public static int bossHP;
        public static Vector3 bossPos;

        void Start()
        {
            bossHP = _instance.Health;
            bossPos = _instance.transform.position;
        }

        protected override void genSound()
        {
            AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/boss_appear");
            AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
            _instance = this;
        }
        
        void Update()
        {
            bossHP = _instance.Health;
            bossPos = _instance.transform.position;
        }
    }
}