using UnityEngine;

namespace gc
{
    public class Enemy2: Enemy{
    
        //change sound for player generation
        protected override void genSound()
        {
			AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/enemy_appear_2");
			AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);            
        }
        //change sound for player eliminate
        protected override void ApplyDamage(int damage) {
			Health -= damage;
			if (Health <= 0) {
				AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/enemy_destroyed_3");
				AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
			}
		}
    }
}