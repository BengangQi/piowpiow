﻿using UnityEngine;
using System.Collections;

namespace gc
{
    public class Minion : Enemy
    {
        protected override void OnDestroy()
        {
            SpawnTask.minionNum--; 
        }
    }
}
