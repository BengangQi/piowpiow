using UnityEngine;

namespace gc
{
    public class dashEnemyMovement: EnemyMovement{
        protected override void enemyMove(){
            //Debug.Log(distanceToPlayer);
              
        if(distanceToPlayer > 30){
			// Determine the steering force (this is using Craig Reynolds' classic approach. You can find out more here: http://www.red3d.com/cwr/steer/gdc99/)
			Vector2 desiredVelocity = directionToPlayer * MaxSpeed;
			Vector2 steeringForce = desiredVelocity - body.velocity;
			Vector2.ClampMagnitude(steeringForce, MaxSpeed);
            body.AddForce(steeringForce);
			float currentSpeed = body.velocity.magnitude;
			if (currentSpeed > MaxSpeed) {
                body.velocity = Vector2.ClampMagnitude(body.velocity, MaxSpeed);
			}

			// make the body face in the direction of movement
			float angle = Mathf.Rad2Deg * (Mathf.Atan2(body.velocity.y, body.velocity.x)) + 180; // The extra 180° are because the enemy assets are pointing to the left
			body.MoveRotation(angle);
        }else
        //use dash
        {
            body.AddForce(transform.forward);  
        }               
        }
    }      

}