﻿using UnityEngine;
using System;

namespace gc {

	public class Player : MonoBehaviour {

		public static Player Instance { get; private set; }

		public int Health = 100;

        private FSM<Player> _states;

        public static Vector2 playerPos;

        public Player()
        {
            _states = new FSM<Player>(this);
            _states.TransitionTo<Normal>();
        }

        //Normal State, this is default state
        private class Normal : FSM<Player>.State
        {
            public override void Init()
            {
                //Debug.Log("Normal");
            }

            public override void Update()
            {
                // if player hold Z button
                if (Input.GetKeyDown(KeyCode.Z))
                {
                    TransitionTo<Charging>();
                }
            }
        }

        // When player is holding Z button, beam begins to charge
        private class Charging : FSM<Player>.State
        {
            float startTime;
            public override void Init()
            {
                //Debug.Log("Charging");
                startTime = Time.time;
            }

            public override void Update()
            {
                // Turn off bullet firing
                Player.Instance.GetComponent<Gun>().enabled = false;

                if (Input.GetKeyUp(KeyCode.Z))
                {
                    float power = (Time.time - startTime) * 3;

                    Vector2 _firingDirection = Vector2.up;
                    // Aim the gun wherever the mouse is pointed
                    Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Vector2 shipPosition = Player.Instance.transform.position;
                    Vector2 offsetToMouse = mousePosition - shipPosition;

                    // Only update the firing direction when the mouse isn't directly on top of the ship
                    // (this is so microscopic changes in mouse position don't change the firing direction)
                    if (offsetToMouse.magnitude > 0.01f)
                    {
                        Vector3 mouseDirection = offsetToMouse.normalized;
                        _firingDirection = mouseDirection;
                    }

                    Bullet.Fire(_firingDirection, Player.Instance.transform.position, power);
                    TransitionTo<Normal>();
                }  
            }
        }

        void Awake() {
			Instance = this;
            playerPos = Instance.transform.position;

		}

		void Update() {
            // update states
            _states.Update();
            playerPos = Instance.transform.position;


            if (Health <= 0) {
				Destroy(gameObject);
			}

		}

		void ApplyDamage(int damage) {
			Health -= damage;
			if (Health <= 0) {
				AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/player_destroyed");
				AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
			}
		}

		void OnDestroy() {
			Instance = null;
            EventManager.Instance.Fire(new PlayerDieEvent());
        }

    }

}


