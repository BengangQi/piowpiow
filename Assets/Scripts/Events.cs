﻿using UnityEngine;
using System.Collections;

public class EnemyDieEvent: Event
{
    public readonly int score;

    public EnemyDieEvent(int score)
    {
        this.score += score;
    }
}

public class PlayerDieEvent: Event
{
    public PlayerDieEvent()
    {
        Debug.Log("PlayerDie");
    }
}